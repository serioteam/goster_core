# -*- coding: utf-8 -*-
import os, sys, subprocess, re
from PIL import Image

# 1 get video length in seconds
# 2 prepare ghosts
# 3 prepare commands
# run whole commands with temp files

inputFile = sys.argv[1]
outputFile = sys.argv[2]

def getLengthInSeconds(filename):
	result = subprocess.Popen(["ffprobe", filename],
		stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
	rawLength = [x for x in result.stdout.readlines() if "Duration" in x]
	print rawLength
	pattern = "Duration: ([0-9]+):([0-9]+):([0-9]+).*, start"
	matches = re.search(pattern, rawLength[0])
	hours = int(matches.group(1))
	minutes = int(matches.group(2))
	seconds = int(matches.group(3))
	return hours * 60 * 60 + minutes * 60 + seconds

lengthInSeconds = getLengthInSeconds(inputFile)
print lengthInSeconds

# def getCommand(length):
# 	array = []
# 	for x in xrange(0,length):
# 		outputGhost = 'outputGhost{}.png'.format(x)
# 		img = Image.open('ghost.png')
# 		wsize = int((float(img.size[1])/(x+1)))
# 		hsize = int((float(img.size[1])/(x+1)))
# 		img = img.resize((basewidth,hsize), Image.ANTIALIAS)
# 		img.save(outputGhost) 
# 		array.append("overlay=(W-w)/2:(H-h)/2:enable='between(t,{},{})'".format(x, x + 1))
# 	return "; ".join(array)

# commands = getCommand(lengthInSeconds)
# ghostFile = ghost.png
def getCommands(inputFile, outputFile, lengthInSeconds):
	outFiles = []
	array = []
	for x in xrange(0,3):
		print x
	for x in xrange(0, lengthInSeconds):
		outputGhost = 'outputGhost{}.png'.format(x)
		img = Image.open('ghost.png')
		wsize = int((float(img.size[1])/(x+1)))
		hsize = int((float(img.size[1])/(x+1)))
		img = img.resize((wsize,hsize), Image.ANTIALIAS)
		img.save(outputGhost)
		outFile = ""
		if x == lengthInSeconds - 1:
			outFile = outputFile
		else:
			outFile = "outFile{}.mp4".format(x)
		outFiles.append(outFile)
		inFile = ""
		if x == 0:
			inFile = inputFile
		else:
			inFile = outFiles[x - 1]
		array.append("ffmpeg -i {} -i {} -filter_complex \"[0:v][1:v]overlay=(W-w)/2:(H-h)/2:enable='between(t,{},{})'\" -pix_fmt yuv420p -c:a copy {}; ".format(inFile, outputGhost, x, x+1, outFile)) 
		# array.append("overlay=(W-w)/2:(H-h)/2:enable='between(t,{},{})'".format(x, x + 1))
	return array

commands = getCommands(inputFile, outputFile, lengthInSeconds)

for x in xrange(0, len(commands)):
	# command = "ffmpeg -i {} -i {} -filter_complex \"[0:v][1:v]{}\" -pix_fmt yuv420p -c:a copy {}".format(inputFile, ghostFile, getCommand(lengthInSeconds), outputFile)
	os.system(commands[x])

# command = "ffmpeg -i {} -i {} -filter_complex \"[0:v][1:v]{}\" -pix_fmt yuv420p -c:a copy {}".format(inputFile, ghostFile, getCommand(lengthInSeconds), outputFile)
# print command
# os.system(command)